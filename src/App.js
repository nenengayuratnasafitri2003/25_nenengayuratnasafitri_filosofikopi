import './App.css';
import{
  Nav,
  Navbar,
  NavDropdown,
  NavbarBrand,
  Button,
  Carousel,
  Card,
  CardDeck,
  Row,
  Col,
  Form,
  FormControl
} from 'react-bootstrap';

function Header() {
  return(
      <Navbar fixed="top" bg="light" expand="lg">
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Nav className="ml-5"> 
          <NavDropdown title="Shop" id="basic-nav-dropdown" bg="transparant">
              <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
              <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
              <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
            </NavDropdown>
            <Nav.Link href="#home">Food And Beverage</Nav.Link>
            <NavDropdown title="Collaboration" id="basic-nav-dropdown">
              <NavDropdown.Item href="#action/3.1"><b>FILKOP X ELEMENT BIKE</b></NavDropdown.Item>
              <NavDropdown.Item href="#action/3.2"><b>AROMA RASA KARYA</b></NavDropdown.Item>
            </NavDropdown>
          </Nav>
        <Navbar.Collapse className="justify-content-center" id="basic-navbar-nav">
          <Navbar.Brand href="#home" >
              <img
              src="https://filkopcdn.b-cdn.net/assets/images/logo-font.png"
              width="100"
              height="100"
              alt="React Bootstrap logo"
            /> 
            </Navbar.Brand>
          </Navbar.Collapse>
          <Nav className="mr-5"> 
            <Nav.Link href="#home">Coffee Catering</Nav.Link>
            <Button variant="light">
            <img
              src="https://www.flaticon.com/svg/vstatic/svg/622/622669.svg?token=exp=1610466739~hmac=59b326696c0c4669beca5acc24c14b4c"
              width="20"
              height="20"
              alt="React Bootstrap logo"
            /> 
            </Button>
            <Button variant="light">
            <img
              src="https://www.flaticon.com/svg/vstatic/svg/25/25619.svg?token=exp=1610466883~hmac=35519d307423afd0ddeb72ea10866ac0"
              width="20"
              height="20"
              alt="React Bootstrap logo"
            /> 
            </Button>
            <Button variant="light">
            <img
              src="https://www.flaticon.com/svg/vstatic/svg/74/74472.svg?token=exp=1610467062~hmac=5e141b7da1c67dd01398c0eb056122e1"
              width="20"
              height="20"
              alt="React Bootstrap logo"
            /> 
            </Button>
          </Nav>
      </Navbar>
  );
}

function Welcome(){
  return(
    <Carousel style={{marginTop: "120px"}}>
  <Carousel.Item>
    <img
      className="d-block w-100"
      src="https://filkopcdn.b-cdn.net/upload/banner/1.jpg"
      alt="First slide"
    />
  </Carousel.Item>
  <Carousel.Item interval={500}>
    <img
      className="d-block w-100"
      src="https://filkopcdn.b-cdn.net/upload/banner/2.jpg"
      alt="Third slide"
    />
  </Carousel.Item>
  <Carousel.Item>
    <img
      className="d-block w-100"
      src="https://filkopcdn.b-cdn.net/upload/banner/3.jpg"
      alt="Third slide"
    />
  </Carousel.Item>
</Carousel>
  );
}

function Picture(){
  return(
    <CardDeck style={{marginLeft:"10px", marginRight:"10px", marginTop:"90px", marginBottom:"120px"}}>
    <Card>
    <Card.Img src="https://filkopcdn.b-cdn.net/assets/images/home/arrival.jpg"/>  
  </Card>
  <Card>
    <Card.Img src="https://filkopcdn.b-cdn.net/assets/images/home/best.jpg" />
  </Card>
    <Card>
      <Card.Img src="https://filkopcdn.b-cdn.net/assets/images/home/promo.jpg" />
    </Card>
    <Card>
    <Card.Img src="https://filkopcdn.b-cdn.net/assets/images/home/bag-under.jpg" />
  </Card>
  </CardDeck>
  );
}

function Feature(){
  const Fitur = (props) => {
    return(
      <div className="col-md-10" style={{marginLeft:"100px", marginRight:"100px"}}>
        <div  className="row" style={{margin:"1em 0"}}>
          <img className="col-md-4 col-sm-12 col-xs-12" style={{float: "left"}} src={props.image} />
          <div className="col-md-8 col-sm-12 col-xs-12">
            <a style={{color:"black"}} href={props.image}><h2 style={{fontSize:"25px", fontFamily:"sans-serif"}}><b>{props.title}</b></h2></a>
            <h3 style={{fontSize: "12px", color: "grey"}}>{props.subtitle}</h3>
            <p className="content" style={{color:"grey"}}>
              {props.text}
              <Button variant="secondary" size="sm">Read More</Button>
            </p>
         </div>
        </div>
      </div>
      );
  }

  return (
      <div>
        <Col>
          <Fitur
            title="Roater Rasa Aroma Kaya"
            subtitle="Filosofi Kopi / Artikel - 2020-09-30"
            text="Didirikan pada bulan Mei tahun 2014, Wisangkopi adalah kedai kopi yang berfokus pada penyeduhan manual sebagai amunisi utamanya. Berangkat dari keyakinan bahwa secangkir kopi berkualitas baik masih jarang ditemui di Jakarta, kami membangun kedai pert..."
            image="https://filkopcdn.b-cdn.net/upload/article/thumbnail_wisang.jpg"
          />
        </Col>
        <Col>
          <Fitur
            title="Gayo Kenawat Series: Manis Madu"
            subtitle="Filosofi Kopi / Artikel - 2020-09-30"
            text=""
            image="https://filkopcdn.b-cdn.net/upload/article/thumbnail_madu_kenawat.jpg"
          />
        </Col>
        <Col>
          <Fitur
            title="Turajaji Series: Chill Water"
            subtitle="Filosofi Kopi / Artikel - 2020-09-30"
            text=""
            image="https://filkopcdn.b-cdn.net/upload/article/thumbnail-chill-water.jpg"
          />
        </Col>
        <Col>
          <Fitur
            title="Sunda Aromanis"
            subtitle="Filosofi Kopi / Artikel - 2020-09-30"
            text=""
            image="https://filkopcdn.b-cdn.net/upload/article/thumbnail-sunda-aromanis.jpg"
          />
        </Col>
        <Col>
          <Fitur
            title="Ingatan Kuat dan Bebas Depresi Karena Kopi"
            subtitle="Filosofi Kopi / Artikel - 2020-03-11"
            text="Kopi merupakan salah satu minuman yang paling populer di dunia serta menjadi minuman penyambung hubungan sosial. Kopi tak hanya sebatas minuman untuk meningkatkan semangat dan fokus. Dalam jumlah tertentu, kopi juga bisa memberikan beragam manfaat..."
            image="https://filkopcdn.b-cdn.net/upload/article/goodforbrain.jpg"
          />
        </Col>
        <Col>
          <Fitur
            title="Kopi, si hitam yang lebih dari sekadar minuman"
            subtitle="Filosofi Kopi / Artikel - 2020-02-17"
            text="Dari mana asal kata kopi ? Asal muasalnya dari bahasa Arab 'Qahwah' walau tanaman kopi aslinya dari Ethiopia. Namun baru ditanam secara berkala di Yaman, sehingga kata 'Qahweh' dikenal sebagai yang pertama menggambarkan minuman berwarna hitam ini..."
            image="https://filkopcdn.b-cdn.net/upload/article/photo_2020-02-17_15-44-03.jpg"
          />
        </Col>
        <Col>
          <Fitur
            title="Kopi emang bikin kamu lebih produktif!"
            subtitle="Filosofi Kopi / Artikel - 2020-02-11"
            text="Kopi dan bekerja memang terkait erat, sebab kopi diandalkan untuk mendorong semangat dan konsentrasi kerja sehingga produktifitas terjaga. Tapi kenapa sih kopi sering dianggap penting untuk bekerja? Hal ini didukung penelitian dari London School of H..."
            image="https://filkopcdn.b-cdn.net/upload/article/pct2.png"
          />
        </Col>
      </div>
    );
}

function Link(){
  return (
    <div style={{borderTop:"3px solid black"}} className="mt-5">
    <Row>
    <Col>
          <ul>
          <li style={{listStyleType:"none", marginTop:"15px"}}><a href="#" style={{color:"black"}}>How To Order</a></li>
          <li style={{listStyleType:"none"}}><a href="#" style={{color:"black"}}>How To Pay</a></li>
          <li style={{listStyleType:"none"}}><a href="#" style={{color:"black"}}>Shipping</a></li>
          </ul>
    </Col>
    <Col>
          <ul>
          <li style={{listStyleType:"none", marginTop:"15px"}}><a href="#" style={{color:"black"}}>Contact Us</a></li>
          <li style={{listStyleType:"none"}}><a href="#" style={{color:"black"}}>Return</a></li>
          <li style={{listStyleType:"none"}}><a href="#" style={{color:"black"}}>FAQ</a></li>
          </ul>
    </Col>
    <Col>
          <ul>
          <li style={{listStyleType:"none", marginTop:"15px"}}><a href="#" style={{color:"black"}}>Instagram</a></li>
          <li style={{listStyleType:"none"}}><a href="#" style={{color:"black"}}>Facebook</a></li>
          </ul>
    </Col>
    <Col>
          <ul>
          <li style={{listStyleType:"none", marginTop:"15px", marginRight:"12px"}}><p style={{color:"black", fontSize:"22px"}}><b>Sign up to our newsletter</b></p></li>
           <Form inline>
          <FormControl style={{marginRight: "20px"}}type="text" placeholder="Enter Your Email"/>
          <Button style={{backgroundColor:"black", border:"1px solid black"}}>Join</Button>
          </Form>
          </ul>
    </Col>
    </Row>
    </div>
  );
}

function Footer() {
  return(
    <div style={{border:"black", backgroundColor:"black", marginTop:"10px"}}>
      <Row>
        <Col>
        <p style={{color:"white", fontSize:"14px", textAlign:"left", marginTop:"10px", marginInline:"25px"}}>hello@filosofikopi.id</p>
        </Col>
        <Col>
        <p style={{color:"white", fontSize:"14px", textAlign:"right", marginTop:"10px", marginInline:"25px"}}>Filosofi Kopi ® 2019. Allrights reserved</p>
        </Col>
      </Row>
    </div>
  );
}

function App(){
  return(
      <div>
        <Header/>
        <Welcome />
        <Picture />
        <Feature />
        <Link />
        <Footer />
      </div>
    );
}

export default App;
